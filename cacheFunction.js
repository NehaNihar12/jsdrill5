const cacheFunction = (cb)=>{
    let cache ={};
    return function(){
        let args = Array.prototype.slice.call(arguments);//let args = Array.from(arguments)
        if(args in cache){
            return cache[args];
        }else{
            cache[args] = cb.apply(this,args)
            return cache[args]         
        }
    }
}


module.exports = cacheFunction;