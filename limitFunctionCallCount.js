const limitFunctionCallCount = function(cb,n){
    let count =1
    let callBack =cb;
    const invokeCallback = function (...args){//function()
        if(count<=n){
            count+=1;
            return callBack(...args); //return callBack(...arguments)   
        }
        else{
            callBack=null
            return null;
        }
    }
    return invokeCallback;    
}


module.exports = limitFunctionCallCount;